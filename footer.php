<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package michah-ho
 */

?>
			
            </div>
            <footer id="colophon" class="site-footer" role="contentinfo">
				<div class="site-info">
					<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'michah-ho' ), 'michah-ho', '<a href="http://michahho.com/" rel="designer">Michah Ho</a>' ); ?>
					<span> | <a href="tel:7785588250"><i class="glyphicon glyphicon-phone"></i> (778) 558-8250</a></span><span> | <a href="mailto:michahho@michahho.com"><i class="glyphicon glyphicon-envelope"></i> michahho@michahho.com</a></span>
				</div><!-- .site-info -->
			</footer><!-- #colophon -->
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
