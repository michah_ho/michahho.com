jQuery(document).ready(function() {
    var startchange = $('.skills');
    var offset = startchange.offset().top;
    
    if (startchange.length){
        $(window).scroll(function() { 
            top = offset.top;
            // console.log(offset);
            scroll_start = $(this).scrollTop() +700;
            console.log(scroll_start);
            if(scroll_start >= offset) { // check if user scrolled more than 50 from top of the browser window
              $("#html").addClass("progress-fill"); 
              $("#css").addClass("progress-fill");
              $("#js").addClass("progress-seven");
              $("#php").addClass("progress-six");
              $("#sass").addClass("progress-six");
              $("#bootstrap").addClass("progress-seven");
              $("#wordpress").addClass("progress-seven");
              $("#photoshop").addClass("progress-fill");
              $("#illustrator").addClass("progress-fill");
              $("#git").addClass("progress-six");
            } 
      });
    }
 });


    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $(function() {
    $('.menu-item a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});