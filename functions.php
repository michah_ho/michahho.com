<?php
/**
 * michah-ho functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package michah-ho
 */

if ( ! function_exists( 'michah_ho_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function michah_ho_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on michah-ho, use a find and replace
	 * to change 'michah-ho' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'michah-ho', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'michah-ho' ),
		'secondary' => esc_html__( 'Secondary', 'michah-ho' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'michah_ho_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'michah_ho_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function michah_ho_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'michah_ho_content_width', 640 );
}
add_action( 'after_setup_theme', 'michah_ho_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function michah_ho_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'michah-ho' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'michah_ho_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function michah_ho_scripts() {
	wp_enqueue_style( 'michah-ho-style', get_stylesheet_uri() );

	wp_register_style('googlefont-orbitron', 'http://fonts.googleapis.com/css?family=Orbitron:500', array(), false, 'all');

	wp_enqueue_style( 'googlefont-orbitron');

	wp_enqueue_style( 'bootstrap-custom',  get_template_directory_uri() . "/css/bootstrap-custom.css" );

	wp_enqueue_style( 'simple-sidebar',  get_template_directory_uri() . "/css/simple-sidebar.css" );

	if(is_front_page()){
		wp_enqueue_style( 'video',  get_template_directory_uri() . "/css/video.css" );
	}

	wp_enqueue_style( 'main',  get_template_directory_uri() . "/css/main.css" );

	wp_enqueue_script( 'michah-ho-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'michah-ho-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	 if (!is_admin() && $GLOBALS['pagenow'] != 'wp-login.php') {
        // comment out the next two lines to load the local copy of jQuery
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', false, '1.11.2');
        wp_enqueue_script('jquery');
    }


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'michah_ho_scripts' );

function footer_scripts(){
	wp_enqueue_script( 'bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js');
	if(is_front_page()){
		wp_enqueue_script( 'video', get_template_directory_uri() . '/js/video.js');
		wp_enqueue_script( 'jquery-easing', get_template_directory_uri() . '/js/jquery.easing.min.js');
	}
	wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js');
}

add_action('wp_footer', 'footer_scripts');

// function add_nav_class($output) {
//     $output= preg_replace('/<a/', '<a class="page-scroll"', $output, 1);
//     return $output;
// }
// add_filter('wp_nav_menu', 'add_nav_class');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';

//remove styles from ninja forms 

// remove_action( 'ninja_forms_display_css', 'ninja_forms_display_css');