<?php

 /**
 * This is the template for the Single Portfolio Post
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package michah-ho
 */

get_header();

if (have_posts()) : while (have_posts()) : the_post(); 

if(get_the_post_thumbnail() != ""){
	$post_id = get_the_ID();
	$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );
	?>	
		<div class="row header-banner" style="background-image:url('<?php echo $image_attributes[0];?>');background-size:cover;background-position:center center;">
			<h1><?php echo get_the_title();?></h1>
		</div>
		
	<?php
	}
	else{
	?>
<div class="row header-banner default-bg">
			<h1><?php echo get_the_title();?></h1>
</div>

<?php 
} 
?>
<div class="row portfolio-content">
<div class="site-content container-fluid">
<?php echo get_the_content();?>
</div>
</div>
<?php
endwhile;
endif;
?>
<div class="row" style="background:#111">
<div class="portfolio-content row">
<div class="container-fluid">
 <div id="my-work">
			<h2> My Work </h2>
				<?php
				$args = array( 
					'post_type' => 'portfolio',
					'posts_per_page' => -1,
					'order' => 'ASC',
					'post__not_in' => array($post_id)
					);
				$portfolio_query = new WP_Query( $args );


				if ( $portfolio_query->have_posts() ) :

					/* Start the Loop */
					while ( $portfolio_query->have_posts() ) : $portfolio_query->the_post();
						$post_id = get_the_ID();
						$logo = get_post_meta($post_id, '_portfolioLogo', true);
						?>
						<div class="col-lg-4 col-md-6 col-sm-12 portfolio-item" style="<?php?>">
						<a href="<?php echo get_the_permalink()?>">
							<img src="<?php echo $logo ?>">
						</a>
						
							<h4><a href="<?php echo get_the_permalink()?>" class="portfolio-btn">
							<?php echo the_title(); ?> 
							</a></h4>
						
						</div>
						<?php

					endwhile;
				endif; ?>
		</div>
	</div>
	</div>
</div>
<?php

get_footer();