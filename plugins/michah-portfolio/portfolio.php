<?php
/*
Plugin Name:michah-portfolio
Plugin URI: http://michahho.com/
Description: Declares a plugin that will create a custom post type for portfolios
Version: 1.0
Author: Michah Ho
Author URI: http://michahho.com/
License: GPLv2
*/

add_filter( 'template_include', 'portfolio_include_template_function', 1 );
function portfolio_include_template_function( $template_path ) {
    if ( get_post_type() == 'portfolio' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single-portfolio.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . 'single-portfolio.php';
            }
        }
    }

    return $template_path;
}

add_action( 'init', 'dev_edge_create_portfolio' );
function dev_edge_create_portfolio() {
    register_post_type( 'portfolio',
        array(
            'labels' => array(
                'name' => 'Portfolios',
                'singular_name' => 'Portfolio',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Portfolio',
                'edit' => 'Edit',
                'edit_item' => 'Edit Portfolio',
                'new_item' => 'New Portfolio',
                'view' => 'View',
                'view_item' => 'View Portfolios',
                'search_items' => 'Search Portfolios',
                'not_found' => 'No Portfolios found',
                'not_found_in_trash' => 'No Portfolios found in Trash',
                'parent' => 'Parent Portfolio'
            ),
 
            'public' => true,
            'show_ui' => true,
            "hierarchical" => true,
            'menu_position' => 15,
            'publicly_queryable' => true,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail'),
            'taxonomies' => array( 'portfolio-type' ),
            'menu_icon' => 'dashicons-format-aside',
            'has_archive' => true
        )
    );
}

add_action( 'init', 'create_portfolio_study_taxonomies', 0 );

// create Portfolio taxonomy
function create_portfolio_study_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Portfolio Types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Portfolio Type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Portfolio Types' ),
        'all_items'         => __( 'All Portfolio Types' ),
        'parent_item'       => __( 'Parent Portfolio Types' ),
        'parent_item_colon' => __( 'Parent Portfolio Type:' ),
        'edit_item'         => __( 'Edit Portfolio Type' ),
        'update_item'       => __( 'Update Portfolio Type' ),
        'add_new_item'      => __( 'Add New Portfolio Type' ),
        'new_item_name'     => __( 'New Portfolio Type Name' ),
        'menu_name'         => __( 'Portfolio Type' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'portfolio-type', 'with_front' => false ),
    );

    register_taxonomy( 'portfolio-type', array( 'portfolio-types' ), $args );
}

/* Start Metaboxes */ 

add_action( 'add_meta_boxes', 'add_portfolio_study_metaboxes' );

function add_portfolio_study_metaboxes() {

    add_meta_box('portfolio_meta_box', 'Portfolio Details', 'portfolio_input_screen', 'portfolio', 'normal', 'high');

}

function portfolio_input_screen(){
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="portfolio_meta_noncename" id="portfolio_meta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__ ) ) . '" />';
    // Get the data if its already been entered
    
    $portfolioLogo = get_post_meta($post->ID, '_portfolioLogo', true);

    $stats = get_post_meta($post->ID, '_stats', true);

    $stat_subline = get_post_meta($post->ID, '_stat_subline', true);


    // Image Uploader
    echo "<p><label for='_portfolioLogo'>Logo Upload</label><input type='text' name='_portfolioLogo' id='portfolio-logo' value='" . $portfolioLogo  . "'><input type='button' id='meta-image-button' class='button' value='Please Upload a Logo'></p>";

}

// Save the Metabox Data

function save_events_meta($post_id, $post) {
    
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['portfolio_meta_noncename'], plugin_basename(__FILE__) )) {
    return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    
    $portfolio_meta['_portfolioLogo'] = $_POST['_portfolioLogo'];
    $portfolio_meta['_stats'] = $_POST['_stats'];
    $portfolio_meta['_stat_subline'] = $_POST['_stat_subline'];
    
    // Add values of $portfolio_meta as custom fields
    
    foreach ($portfolio_meta as $key => $value) { // Cycle through the array
        if( $post->post_type != 'portfolio' ) return; // Don't store custom data twice
        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
        if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }

}

add_action('save_post', 'save_events_meta', 1, 2); // save the custom fields

//Add Image Script for Wordpress Image Uploader

function devedge_image_enqueue() {
    global $typenow;
    if( $typenow == 'portfolio' ) {
        wp_enqueue_media();
 
        // Registers and enqueues the required javascript.
        wp_register_script( 'meta-box-image', plugin_dir_url( __FILE__ ) . '/js/meta-box-image.js', array( 'jquery' ) );
        wp_localize_script( 'meta-box-image', 'meta_image',
            array(
                'title' => __( 'Choose or Upload an Image', 'Portfolio Details' ),
                'button' => __( 'Use this image', 'Portfolio Details' ),
            )
        );
        wp_enqueue_script( 'meta-box-image' );
    }
}
add_action( 'admin_enqueue_scripts', 'devedge_image_enqueue' );