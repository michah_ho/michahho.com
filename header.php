<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package michah-ho
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="<?php echo get_home_url() ?>">
                        <img src="/michahho/wp-content/themes/michah-ho/images/logo.png">
                    </a>
                </li>
			<?php
			if(is_front_page()){  
			wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container_class' => 'side-menu') ); 
			}
			else{
			wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_id' => 'secondary-menu', 'container_class' => 'side-menu') ); 	
			}
			?>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

 <div id="page-content-wrapper">
            <header class="container-fluid">
                <div class="row">
                    <nav class="nav navbar-default navbar-fixed-top">
                        
                            <button type="button" id="menu-toggle" class="navbar-toggle collapsed">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                       
                    </nav>
                </div>
            </header>

            <?php if(is_front_page()){ ?>
            
            <div class="homepage-hero-module">
                 <div class="video-container">
                    <div class="filter"></div>
                    <video autoplay loop class="fillWidth">
                        <source src="/michahho/wp-content/themes/michah-ho/video/bg-video.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                        <source src="/michahho/wp-content/themes/michah-ho/video/bg-video.webm" type="video/webm" />
                    </video>
                    <div class="poster hidden">
                        <img src="/michahho/wp-content/themes/michah-ho/video/poster.jpg" alt="video-bg">
                    </div>
                    <div class="container-fluid video-heading">
                        <h1>
                        <?php
                        $description = get_bloginfo( 'description', 'display' );
						echo $description;
                        ?>
                        </h1>
                        <h4> Michah Ho | Front End Web Developer | Web Designer </h4>
                    </div>
                </div>
            </div>
            <?php } ?>

            <div class="container-fluid">