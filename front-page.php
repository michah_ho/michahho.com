<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package michah-ho
 */

get_header(); ?>
<div class="row work-container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		 <div id="my-work">
			<h2> My Work </h2>
				<?php
				$args = array( 
					'post_type' => 'portfolio',
					'posts_per_page' => -1,
					'order' => 'ASC',
					);
				$portfolio_query = new WP_Query( $args );


				if ( $portfolio_query->have_posts() ) :

					/* Start the Loop */
					while ( $portfolio_query->have_posts() ) : $portfolio_query->the_post();
						$post_id = get_the_ID();
						$logo = get_post_meta($post_id, '_portfolioLogo', true);
						?>
						<div class="col-lg-4 col-md-6 col-sm-12 portfolio-item" style="<?php?>">
						<a href="<?php echo get_the_permalink()?>">
							<img src="<?php echo $logo ?>">
						</a>
						
							<h4><a href="<?php echo get_the_permalink()?>" class="portfolio-btn">
							<?php echo the_title(); ?> 
							</a></h4>
						
						</div>
						<?php

					endwhile;

					the_posts_navigation();
				endif; ?>
		<div class="clearfix"></div>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<div class="row skills">
	<div class="container-fluid">
		<div id="skills">
			<h2> Technical Skills </h2>
		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="html" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    HTML
				  </div>
				</div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="css" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    CSS
				  </div>
			</div>
		</div>
		

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="js" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    Javascript
				  </div>
				</div>
		</div>
	

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="php" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    PHP
				  </div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="wordpress" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    Wordpress
				  </div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="photoshop" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    Photoshop
				  </div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="illustrator" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    Illustrator
				  </div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="bootstrap" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    Bootstrap
				  </div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="sass" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    SASS
				  </div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="progress">
				  <div class="progress-bar" id="git" role="progressbar" aria-valuenow="100"
				  aria-valuemin="0" aria-valuemax="100">
				    Git
				  </div>
			</div>
		</div>

	</div>
</div>

<div class="row about">
	<div id="about">
	<h2>When I'm not thinking about websites...</h2>
	<p>
		I am actively pursuing the life of a Martial Artist. You will find me either swinging a bamboo sword or kicking a sandbag. I love Kung Fu Movies and playing Street Fighter. I am also actively studying Japanese! 
	</p>
	<p>
		Currently I am working in Victoria doing web design and development. My career goal is to eventually become a Full-Stack Web Developer.
	</p>
	</div>
</div>

<div class="row contact">
	<div id="contact">
		<div class="container-fluid">
			<h2> Let's Do Something Cool!</h2>
	 		<?php echo do_shortcode('[ninja_forms id=1]');?>
	 	</div>
	</div>
</div>

<?php
// get_sidebar();
get_footer();
